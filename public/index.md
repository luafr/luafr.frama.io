Bienvenue sur le site de la communauté francophone de Lua.

Le site est pour le moment vide mais sera enrichi de documentation sur Lua à destination des débutants ainsi que du manuel traduit en français.

N'hésitez pas à nous contacter via IRC sur le canal [#luafr](http://webchat.freenode.net/?channels=#luafr) de Freenode, ou sur [Framagit](https://framagit.org/luafr/).
